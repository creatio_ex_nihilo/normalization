import math
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d, lagrange

def calc_number_of_roles(prob, die):
    return math.ceil(math.log(abs(prob-1)) / math.log((die-1)/die))

def print_one(die, current_prob, rolls):
    print('to role a certain value on a', die, 'sided die with a probability of', '%.2f'%(current_prob*100), 'you have to role', rolls, 'time(s)')

def print_two(die, current_prob, rolls):
    print(str(int(current_prob*100)).zfill(2), '%', '=>', str(int(rolls)).zfill(3), 'rolls')

def main():
    step_size = 0.001
    steps = int(1/step_size)
    dice = [4, 6, 8, 10, 12, 20, 100]

    for die in dice:
        current_rolls = 0
        tmp_prob = list()
        tmp_rolls = list()
        for i in range(1,steps):
            current_prob = step_size*i
            rolls = calc_number_of_roles(current_prob, die)
            if current_rolls < rolls:
                current_rolls = rolls
                tmp_prob.append(current_prob)
                tmp_rolls.append(current_rolls)
        #f1 = interp1d(tmp_rolls, tmp_prob, kind='nearest')
        #f1 = lagrange(tmp_rolls, tmp_prob)
        plt.plot(tmp_rolls, tmp_prob, label='%d-sided die'%die, drawstyle='steps')
        plt.axis([0, 100, 0, 1.01])

    #print(f1)

    plt.axhline(y=.5, color='red', linewidth=.5, linestyle='dashed')
    plt.xlabel('rolls')
    plt.ylabel('probability')
    plt.legend()
    plt.grid(b=True, which='major', color='#666666', linestyle='dashed', alpha=0.4)
    plt.minorticks_on()
    plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
    plt.show()

if __name__ == '__main__':
    main()