def calc_stuff(elements, normalization_base):
    out = dict()
    alpha = normalization_base / sum(elements)
    out['length'] = len(elements)
    n = 0
    i = 0
    for n in elements:
        tmp = dict()
        tmp['element'] = n
        tmp['normalized_element'] = '%.8f'%(alpha * n)
        out[i] = tmp
        i = i + 1
    out['alpha'] = '%.12f'%alpha
    return out

def centered_string(string, length):
    return string.rjust(int(length/2 + (len(string)/2))).ljust(length)

def print_stuff(elements):
    # length of heading is 20
    l = 20
    print('+--------------------+--------------------+')
    print('|', centered_string('alpha = {}'.format(elements['alpha']), (l*2)+1), '|', sep='')
    print('+--------------------+--------------------+')
    print('|      element       | normalized element |')
    print('+--------------------+--------------------+')
    i = 0
    s = 0
    for i in range(0, elements['length']):
        tmp = elements[i]
        print('|', centered_string(str(tmp['element']), l), '|', centered_string(str(tmp['normalized_element']), l), '|', sep='')
        s = s + float(tmp['normalized_element'])
    print('+--------------------+--------------------+')
    print('|        sum         |', centered_string(str(s), l),'|', sep='')
    print('+--------------------+--------------------+')

elements = [0.000366941, 0.001717159]
normalization_base = 1

print_stuff(calc_stuff(elements, normalization_base))