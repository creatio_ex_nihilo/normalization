import math, sys

def entropy(p, n):
    return - (p/(p+n)) * math.log2(p/(p+n)) - (n/(p+n)) * math.log2(n/(p+n))

def main():
    p = 4
    n = 0
    print(entropy(p, n))

if __name__ == '__main__':
    print(entropy(int(sys.argv[1]), int(sys.argv[2])))