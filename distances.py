import math, numpy

def pretty_print_float(f):
    return '{:09.5f}'.format(f)

def euclid(x, y):
    out = 0.0
    for i in range(len(x)):
        out = out + math.pow(x[i] - y[i], 2)
    return math.sqrt(out)

def squared_euclid(x, y):
    out = 0.0
    for i in range(len(x)):
        out = out + math.pow(x[i] - y[i], 2)
    return out

def manhattan(x, y):
    out = 0.0
    for i in range(len(x)):
        out = out + abs(x[i] - y[i])
    return out

def cosine_simularity(x, y):
    sum_mult = 0.0
    sum_x = 0.0
    sum_y = 0.0
    for i in range(len(x)):
        sum_x = sum_x + math.pow(x[i], 2)
        sum_y = sum_y + math.pow(y[i], 2)
        sum_mult = sum_mult + x[i]*y[i]
    return sum_mult / (math.sqrt(sum_x) * math.sqrt(sum_y))

def print_matrix(name, outputs):
    print('--------------------------------------')
    print(name)
    for i in outputs:
        string = ''
        for j in i:
            string = string + pretty_print_float(j) + ' '
        print(string)
    print('--------------------------------------')

def do_calc_and_print(inputs, method):
    outputs = list()
    sort_stuff = set()
    for current_top in inputs:
        tmp = list()
        for current in inputs:
            val = globals()[method](current_top, current)
            tmp.append(val)
            sort_stuff.add(val)
        outputs.append(tmp)
    print_matrix(method, outputs)
    print(', '.join(map(pretty_print_float, sorted(sort_stuff))))

def do_initial_k_means_cluster(inputs, cluster_seeds):
    out = dict()
    for element in inputs:
        tmp = dict()
        for seed in cluster_seeds:
            tmp[str(seed)] = (squared_euclid(seed, element))
        out[str(element)] = tmp
    
    for result in out.items():
        print(result)

def calc_centroid(inputs):
    transposed = list(map(list, zip(*inputs)))
    # i'm not sure what kind of calc to do, sooooo ... just a simple avg
    out = list()
    for e in transposed:
        out.append(numpy.mean(e))
    print(out)

def main():
    inputs = [[3,1,2], [0,2,1], [1,3,2], [6,6,6], [5,6,4], [7,8,9], [9,7,7]]
    do_calc_and_print(inputs, 'euclid')
    do_calc_and_print(inputs, 'squared_euclid')
    do_calc_and_print(inputs, 'manhattan')
    do_calc_and_print(inputs, 'cosine_simularity')

    #cluster_seeds = [[2,2,2], [8,8,8]]
    #do_initial_k_means_cluster(inputs, cluster_seeds)
    #calc_centroid([[3,1,2], [0,2,1], [1,3,2], [5,6,4]])
    #calc_centroid([[6,6,6], [7,8,9], [9,7,7]])
    #cluster_seeds = [[2.25,3,2.25], [7.33,7,7.33]]
    #do_initial_k_means_cluster(inputs, cluster_seeds)
    #calc_centroid([[3,1,2], [0,2,1], [1,3,2]])
    #calc_centroid([[6,6,6], [5,6,4], [7,8,9], [9,7,7]])
    #cluster_seeds = [[1.33,2,1.66], [6.75,6.75,6.5]]
    #do_initial_k_means_cluster(inputs, cluster_seeds)

if __name__ == '__main__':
    main()