from dice import calc_number_of_roles
import random, math, threading, queue, time

class RollingThread (threading.Thread):
    def __init__(self, val, die, min_rounds, max_rounds, min_rolls, q, q_l):
        threading.Thread.__init__(self)
        self.val = val
        self.die = die
        self.min_rounds = min_rounds
        self.max_rounds = max_rounds
        self.min_rolls = min_rolls
        self.q = q
        self.q_l = q_l
    
    def run(self):
        #cnt = 0
        for i in range(self.min_rounds, self.max_rounds):
            cnt = 0
            for j in range(self.min_rolls):
                if random.randint(1, self.die) == self.val:
                    cnt = 1
                    break
            self.q_l.acquire()
            self.q.put(cnt)
            self.q_l.release()

class PrintThread (threading.Thread):
    def __init__(self, rounds, prob, start_time, q, q_l):
        threading.Thread.__init__(self)
        self.rounds = rounds
        self.prob = prob
        self.start_time = start_time
        self.q = q
        self.q_l = q_l

    def run(self):
        out_cnt = 0
        cnt = 0
        while cnt < self.rounds:
            self.q_l.acquire()
            out_cnt = out_cnt + self.q.get()
            self.q_l.release()
            cnt = cnt + 1
            print_percentage(cnt, self.rounds)
        print()
        print('from the at least expected', '{:.2f}'.format(self.prob*100), '%,','{:.2f}'.format((out_cnt/self.rounds)*100), '% were reached')
        print('elapsed seconds:', time.time() - self.start_time)

def print_percentage(reached, total):
    max_char_cnt = 38
    percentage = reached/total
    current_char_cnt = math.floor(percentage*max_char_cnt)
    print('[', '#'*(current_char_cnt), '.'*(max_char_cnt - current_char_cnt), '] ', '{:06.2f}'.format(percentage*100), '% of rolls done', sep='', end='\r')

def main():
    # val needs to be smaller than die
    val = 2
    die = 6
    # prob needs to be between 0 and 1
    prob = 0.5
    # rounds needs to be bigger than thread_cnt
    rounds = 1528391
    thread_cnt = 8

    q_l = threading.Lock()
    q = queue.Queue()

    min_rolls = calc_number_of_roles(prob, die)
    rolls_per_thread = math.floor(rounds/thread_cnt)
    remaining_rolls = rounds % thread_cnt

    print('given a', die, 'sided die and a minimum probability of', '{:.2f}'.format(prob*100), '% to roll a', val, ',')
    print(min_rolls, 'rolls are needed to achieve this on average, which will be simulated with', rounds, 'rounds of said rolls')

    pt = PrintThread(rounds, prob, time.time(), q, q_l)

    rr = 0
    #threads = list()
    for i in range(thread_cnt):
        if i == thread_cnt - 1:
            rr = remaining_rolls
        tmp = i*rolls_per_thread

        t = RollingThread(val, die, tmp, tmp+rolls_per_thread+rr, min_rolls, q, q_l)
        t.start()
        #threads.append(t)

    pt.start()

    """ for t in threads:
        t.join() """

if __name__ == '__main__':
    # i could speed up this script by removing the print thread, 
    # which fights with all the rolling threads for the lock.
    # so letting the threads do their stuff and returning only the 
    # final value into the queue would be quite a performance boost
    # but ... i mean, a progressbar is pretty cool xD
    main()
