import math
import os
import random
import copy
import json
from termcolor import colored

class Elo:
    # start elo with 1000 (1400 because of "The Social Network")
    # normally would be estimated (but this seems unfair and somewhat arbitrary)
    default = 1400
    # default = 20; top players (elo > 2400) = 10; less than 30 games = 40
    k = [20, 10, 40]
    # modifications according to Jeff Sonas
    #k = [20, 24, 40]
    #d = 480
    # chosen by Arpad Elo
    d = 400
    # at what elo is a player considered a top player
    top = 2400
    # how long is the honeymoon phase (in #games)
    honeymoon_phase = 30
        
    @staticmethod
    def recalc_elo(old_elo, result, expected_value, k=k[0]):
        #return old_elo + k * (result - expected_value)
        return old_elo + Elo.calc_difference(result, expected_value, k)

    @staticmethod
    def calc_difference(result, expected_value, k=k[0]):
        tmp = k * (result - expected_value)
        return math.floor(tmp + 0.5)

    @staticmethod
    def calc_expected_value(elo_one, elo_two):
        if abs(elo_one - elo_two) <= Elo.d:
            return 1 / (1 + math.pow(10, (elo_one - elo_two)/Elo.d))
        else:
            # if the difference between elo_one and elo_two is bigger than Elo.d
            # use Elo.d (and -Elo.d) instead
            if elo_one < elo_two:
                # 1 / (1 + math.pow(10, -Elo.d/Elo.d))
                return 0.9090909090909091
            else:
                # 1 / (1 + math.pow(10, Elo.d/Elo.d))
                return 0.09090909090909091

class Player:
    def __init__(self, name, elo=Elo.default, nr_of_games=0):
        self.name = name
        self.elo = int(elo)
        self.nr_of_games = nr_of_games

    def update_elo(self, elo):
        # kaufmaennisches runden
        # aka
        # round half away from zero
        self.elo = math.floor(elo + 0.5)

    def get_k(self):
        if self.elo > Elo.top:
            return Elo.k[1]
        elif self.nr_of_games < Elo.honeymoon_phase:
            return Elo.k[2]
        else:
            return Elo.k[0]

    def increase_nr_of_games(self):
        self.nr_of_games += 1

    def __str__(self):
        #return self.name + ' (' + str(self.elo) + ') [' + str(self.nr_of_games) + '] {' + str(self.get_k()) + '} '
        return self.name + '\t(' + str(self.elo) + ')\t[' + str(self.nr_of_games) + ']\t{' + str(self.get_k()) + '} '

    def __hash__(self):
        return hash(self.name)

class Match:
    win = 1
    tie = 0.5
    loss = 0

    def __init__(self, player_one, player_two):
        self.player_one = player_one
        self.player_two = player_two
        self.players = [self.player_one, self.player_two]
        self.already_taken = False
        self.already_finished = False

    def set_result(self, player_one_result):
        self.player_one_result = player_one_result
        self.player_two_result = abs(player_one_result - 1)
        self.player_one.increase_nr_of_games()
        self.player_two.increase_nr_of_games()

    def get_result(self):
        if(self.player_one_result == Match.win):
            return self.player_one.name + ' wins'
        elif(self.player_one_result == Match.tie):
            return self.player_one.name + ' & ' + self.player_two.name + ' tie'
        elif(self.player_one_result == Match.loss):
            return self.player_two.name + ' wins'

    def update_elos(self):
        player_A_e = Elo.calc_expected_value(self.player_two.elo, self.player_one.elo)
        player_B_e = Elo.calc_expected_value(self.player_one.elo, self.player_two.elo)
        print(self.player_one.name, Elo.calc_difference(self.player_one_result, player_A_e, self.player_one.get_k()))
        print(self.player_two.name, Elo.calc_difference(self.player_two_result, player_B_e, self.player_two.get_k()))
        self.player_one.update_elo(Elo.recalc_elo(self.player_one.elo, self.player_one_result, player_A_e, self.player_one.get_k()))
        self.player_two.update_elo(Elo.recalc_elo(self.player_two.elo, self.player_two_result, player_B_e, self.player_two.get_k()))

    def __str__(self):
        tmp = str(self.player_one) + 'vs. ' + str(self.player_two)
        if self.already_finished:
            tmp += '=> ' + self.get_result()
        return tmp

class MatchupCreator:
    def __init__(self, players):
        self.players = list(set(players))
        # checking after set() because we might lose some player duplicates
        if len(self.players) < 5:
            raise ValueError("there must be at least 5 players")
        self.matchups = self.create_matchups()

    def create_matchups(self):
        matchups = list()
        # players - 1 because we don't need to create a matchup with just one player
        for i in range(len(self.players) - 1):
            current_player = self.players[i]
            for j in range(i + 1, len(self.players)):
                tmp = [current_player, self.players[j]]
                # only shuffled for the case in which there are sides which have to be random
                # (for example a ping pong table with one "good" side)
                random.shuffle(tmp)
                matchups.append(Match(tmp[0], tmp[1]))
        return matchups

    @staticmethod
    def create_matchups_static(players):
        matchups = list()
        ps = list(set(players))
        # players - 1 because we don't need to create a matchup with just one player
        for i in range(len(ps) - 1):
            current_player = ps[i]
            for j in range(i + 1, len(ps)):
                tmp = [current_player, ps[j]]
                # only shuffled for the case in which there are sides which have to be random
                # (for example a ping pong table with one "good" side)
                random.shuffle(tmp)
                matchups.append(Match(tmp[0], tmp[1]))
        return matchups

    def get_matchups(self):
        return self.matchups

    def sort_matchups(self):
        # constant for the loop max after which most of the time a possible sorting is found
        loop_max = 5
        smus = []
        found_one = False
        while not found_one:
            # (re)set 'already_taken'
            self.reset_taken_matchups()

            # offset (shuffle the matchups)
            random.shuffle(self.matchups)

            smus = self.try_to_find_matchups(loop_max)
            # only if all matchups are in the sorted matchup list
            if len(smus) == len(self.matchups):
                found_one = True
        return smus

    def try_to_find_matchups(self, loop_max):
        # offset (take one of the pre-shuffled matchups)
        self.matchups[0].already_taken = True
        smus = [self.matchups[0]]
        # init cnt
        cnt = self.get_nontaken_matchups_len()

        current_matchup = smus[0]
        loop_cnt = 0
        # if loop_max is reached the last chosen matchup doesn't lead to a
        # possible next matchup, so start over
        while cnt != 0 and loop_max > loop_cnt:
            loop_cnt += 1
            for matchup in self.matchups:
                if not matchup.already_taken and self.cmp_matchups(current_matchup, matchup) == 1:
                    # choose matchup if none of the players participated in the previous one
                    matchup.already_taken = True
                    smus.append(matchup)
                    current_matchup = smus[-1]
                    # update cnt
                    cnt = self.get_nontaken_matchups_len()
        return smus

    def cmp_matchups(self, mu_one, mu_two):
        for player in mu_one.players:
            if player in mu_two.players:
                return 0
        return 1

    def reset_taken_matchups(self):
        for matchup in self.matchups:
            matchup.already_taken = False

    def reset_finished_matchups(self):
        for matchup in self.matchups:
            matchup.already_finished = False

    def get_nontaken_matchups_len(self):
        cnt = 0
        for matchup in self.matchups:
            if not matchup.already_taken:
                cnt += 1
        return cnt

    def get_nonfinished_matchups_len(self):
        cnt = 0
        for matchup in self.matchups:
            if not matchup.already_finished:
                cnt += 1
        return cnt

def save_to_json(c):
    f = 'players_elo'
    dir_path = os.path.dirname(os.path.realpath(__file__))
    #dir_path  + "\\" + 
    with open(f + ".json", 'w') as file:
        file.write(json.dumps(c.players, default=lambda o: o.__dict__, sort_keys=True, indent=4))

def load_from_json():
    f = 'players_elo'
    players = None
    dir_path = os.path.dirname(os.path.realpath(__file__))
    #dir_path  + "\\" + 
    with open(f + ".json", 'r') as file:
        players = json.load(file)
    return players

def create_player_from_json(player_json):
    return Player(player_json['name'], player_json['elo'], player_json['nr_of_games'])

def eco_test():
    A = Player('A', 1500)
    B = Player('B', 1010)
    A_e = Elo.calc_expected_value(B.elo, A.elo)
    B_e = Elo.calc_expected_value(A.elo, B.elo)
    print(A_e)
    print(B_e)
    A_elo = Elo.recalc_elo(A.elo, Match.loss, A_e)
    B_elo = Elo.recalc_elo(B.elo, Match.win, B_e)
    print(A_elo)
    print(B_elo)

def main_interactive():
    ps = set()
    ps.add(Player('Chris'))
    ps.add(Player('Dom'))
    ps.add(Player('Mathias'))
    ps.add(Player('Markus'))
    ps.add(Player('Tory'))

    c = MatchupCreator(ps)
    sorted_matchups = c.sort_matchups()
    # game loop
    for i in range(len(sorted_matchups)):
        tmp_match = sorted_matchups[i]
        print(colored(tmp_match, 'yellow'))
        # next up
        # print(colored(sorted_matchups[i+1], 'yellow'))
        correct_statement = False
        while not correct_statement:
            result_of_match = input(colored('Did ', 'green') + colored(str(tmp_match.player_one), 'red') + colored('win (w), tie (t) or lose (l)? \r\nType the letter in the brackets...\r\n', 'green'))
            if result_of_match == 'w':
                tmp_match.set_result(Match.win)
                correct_statement = True
            elif result_of_match == 't':
                tmp_match.set_result(Match.tie)
                correct_statement = True
            elif result_of_match == 'l':
                tmp_match.set_result(Match.loss)
                correct_statement = True
            else:
                print('Please use the correct letter (w), (t) or (l)')
        tmp_match.already_finished = True
        tmp_match.update_elos()
        print(colored(tmp_match, 'yellow'))
        print(50*'-')
    # sort players by elo
    c.players.sort(key=lambda x: x.elo, reverse=True)
    for p in c.players:
        print(colored(p, 'yellow'))
    save_to_json(c)

def main():
    ps = set()
    ps.add(Player('Chris'))
    ps.add(Player('Dom'))
    ps.add(Player('Mathias'))
    ps.add(Player('Markus'))
    ps.add(Player('Tory'))

    c = MatchupCreator(ps)
    possible_results = [Match.win, Match.loss, Match.tie]
    simulated_runs = 400
    while simulated_runs > 0:
        sorted_matchups = c.sort_matchups()
        # game loop
        for tmp_match in sorted_matchups:
            print(colored(tmp_match, 'yellow'))
            # 3% for a draw - just because
            player_one_e = round(Elo.calc_expected_value(tmp_match.player_two.elo, tmp_match.player_one.elo) - 0.015, 2)
            player_two_e = round(Elo.calc_expected_value(tmp_match.player_one.elo, tmp_match.player_two.elo) - 0.015, 2)
            tmp_match.set_result(random.choices(possible_results, cum_weights=(player_one_e, player_one_e + player_two_e, 1), k=1)[0])
            tmp_match.already_finished = True
            tmp_match.update_elos()
            print(colored(tmp_match, 'green'))
            print(50*'-')
            """ try:
                check_total_elo(c.players)
            except Exception as ex:
                print(ex) """
        c.reset_finished_matchups()
        simulated_runs -= 1
    # sort players by elo
    c.players.sort(key=lambda x: x.elo, reverse=True)
    print(colored('Name\tELO\t#games\tk', 'red'))
    total_elo = 0
    for p in c.players:
        total_elo += p.elo
        print(colored(p, 'yellow'))
    print('total elo', total_elo)

def check_total_elo(players):
    start_elo = Elo.default * len(players)
    total_elo = 0
    for p in players:
        total_elo += p.elo
    if start_elo != total_elo:
        raise Exception("elo pool isn't correct")


def cli():
    r_or_p = None
    # game_name
    game_name = None
    # add_player <name>
    ps = set()
    # start
    # save_to_json
    # load_from_json
    # next
    while True:
        m = input('name of the game (n), random or with pause (r), add player (a), start (s), save to json (sv), load from json (ld)\r\n')
        if m == 'n':
            game_name = input('enter name\r\n')
        elif m == 'r':
            while r_or_p == None:
                tmp_r_or_p = input('should the matchups occur randomly (r) or with an one-game pause for each player (p) [only possible >= 5 players]\r\n')
                if tmp_r_or_p == 'r':
                    r_or_p = True
                elif tmp_r_or_p == 'p':
                    r_or_p = False
                else:
                    print('please use the correct string (r) or (p)')
        elif m == 'a':
            print(" ")
        elif m == 's':
            # if len(ps) 
            print(" ")
        elif m == 'sv':
            # todo
            print(" ")
        elif m == 'ld':
            # todo
            print(" ")
        else:
            print('please use the correct string (n), (r), (a), (s), (sv) or (ld)')

if __name__ == '__main__':
	main()
